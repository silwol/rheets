use std::collections::BTreeMap;
use cell;

pub struct Worksheet {
    pub name: String,
    pub cells: BTreeMap<(u64, u64), cell::Cell>,
}

impl Worksheet {
    pub fn new_with_name(name: &str) -> Worksheet {
        Worksheet {
            name: name.to_string(),
            cells: BTreeMap::new(),
        }
    }
}
