extern crate failure;

pub mod cell;
pub mod document;
pub mod style;
pub mod worksheet;

use std::fs::{File, OpenOptions};
use std::io::{Read, Seek, Write};
use std::path::Path;

type Result<T> = std::result::Result<T, failure::Error>;

pub trait FileFormat {
    fn preferred_file_extension() -> String;

    fn export<W>(
        document: &document::Document,
        write: &mut W,
    ) -> Result<()>
    where
        W: Write + Seek;

    fn export_to_path<P>(
        document: &document::Document,
        path: &P,
    ) -> Result<()>
    where
        P: AsRef<Path>,
    {
        Self::export(document, &mut File::create(path)?)
    }

    fn import<R>(read: &mut R) -> Result<document::Document>
    where
        R: Read + Seek;

    fn import_from_path<P>(path: &P) -> Result<document::Document>
    where
        P: AsRef<Path>,
    {
        Self::import(&mut OpenOptions::new().read(true).open(path)?)
    }
}
