extern crate rgb;

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum FontWeight {
    Normal,
    Bold,
}

impl Default for FontWeight {
    fn default() -> Self {
        FontWeight::Normal
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum BorderType {
    Solid(u32),
    Double(u32, u32, u32),
    DashDotDot(u32),
    NoBorder,
}

impl Default for BorderType {
    fn default() -> Self {
        BorderType::NoBorder
    }
}

#[derive(Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct BorderStyle {
    pub border_type: BorderType,
    pub color: rgb::RGB8,
}

impl BorderStyle {
    pub fn with_type(self, border_type: BorderType) -> BorderStyle {
        BorderStyle {
            border_type,
            color: self.color,
        }
    }

    pub fn with_color(self, color: rgb::RGB8) -> BorderStyle {
        BorderStyle {
            border_type: self.border_type,
            color,
        }
    }
}

#[derive(Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct DirectionBorderStyles {
    pub top: BorderStyle,
    pub left: BorderStyle,
    pub right: BorderStyle,
    pub bottom: BorderStyle,
}

impl DirectionBorderStyles {
    pub fn new_with(
        top: BorderStyle,
        left: BorderStyle,
        right: BorderStyle,
        bottom: BorderStyle,
    ) -> DirectionBorderStyles {
        DirectionBorderStyles {
            top,
            left,
            right,
            bottom,
        }
    }
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum BorderPlacement {
    Combined(BorderStyle),
    Separate(DirectionBorderStyles),
}

impl Default for BorderPlacement {
    fn default() -> Self {
        BorderPlacement::Combined(BorderStyle::default())
    }
}
