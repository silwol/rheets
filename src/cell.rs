extern crate rgb;

use std::rc::Rc;
use style;
use self::rgb::RGB8;

pub enum Value {
    Float(f64),
    Text(String),
    Bool(bool),
    Empty(),
}

#[derive(Default, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Style {
    pub font_color: Option<RGB8>,
    pub background_color: Option<RGB8>,
    pub font_weight: style::FontWeight,
    pub borders: style::BorderPlacement,
    pub font_name: Option<String>,
    pub font_size: Option<usize>,
}

impl Style {
    pub fn with_font_color(mut self, color: Option<RGB8>) -> Style {
        self.font_color = color;
        self
    }

    pub fn with_background_color(mut self, color: Option<RGB8>) -> Style {
        self.background_color = color;
        self
    }

    pub fn with_font_name(mut self, font_name: Option<String>) -> Style {
        self.font_name = font_name;
        self
    }

    pub fn with_font_weight(
        mut self,
        font_weight: style::FontWeight,
    ) -> Style {
        self.font_weight = font_weight;
        self
    }

    /// Add a font size. Note that the font size is given in 100 times the
    /// required font size in pt, so for 14.4pt, pass 1440.
    pub fn with_font_size(mut self, font_size: Option<usize>) -> Style {
        self.font_size = font_size;
        self
    }

    pub fn with_border(
        mut self,
        border_style: style::BorderStyle,
    ) -> Style {
        self.borders = style::BorderPlacement::Combined(border_style);
        self
    }

    pub fn with_borders(
        mut self,
        top: style::BorderStyle,
        left: style::BorderStyle,
        right: style::BorderStyle,
        bottom: style::BorderStyle,
    ) -> Style {
        self.borders = style::BorderPlacement::Separate(
            style::DirectionBorderStyles::new_with(
                top,
                left,
                right,
                bottom,
            ),
        );
        self
    }
}

pub struct Content {
    pub value: Value,
    pub formula: String,
}

pub struct Cell {
    pub content: Option<Content>,
    pub style: Option<Rc<Style>>,
}

impl Cell {
    pub fn with_style(mut self, style: &Rc<Style>) -> Cell {
        self.style = Some(style.clone());
        self
    }

    pub fn new_with_text(value: &str, formula: &str) -> Cell {
        Cell {
            content: Some(Content {
                value: Value::Text(value.to_string()),
                formula: formula.to_string(),
            }),
            style: None,
        }
    }

    pub fn new_with_float(value: f64, formula: &str) -> Cell {
        Cell {
            content: Some(Content {
                value: Value::Float(value),
                formula: formula.to_string(),
            }),
            style: None,
        }
    }

    pub fn new_with_bool(value: bool, formula: &str) -> Cell {
        Cell {
            content: Some(Content {
                value: Value::Bool(value),
                formula: formula.to_string(),
            }),
            style: None,
        }
    }
}
