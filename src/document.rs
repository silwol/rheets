use std::vec::Vec;
use worksheet;

#[derive(Default)]
pub struct Document {
    pub sheets: Vec<worksheet::Worksheet>,
}
